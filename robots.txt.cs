// threading.cs created with MonoDevelop
// User: shen139 at 22:26 19/06/2008
//

using System;
using System.Text.RegularExpressions;
using GlobalVars;
using nsGlobalOutput;

namespace GlobalVars
{
	public class robotstxtVars
	{
		public static int robotstxtCrawlDelay;
	}
}

namespace OpenWebSpiderCS
{
	public class robots
	{
		public robots()
		{
			output.write(" + Parsing robots.txt...");
			robotstxtVars.robotstxtCrawlDelay = 0;

			// svuota la lista dei Disallow
			robotsTxtDisallows.init();
		}

		public bool parseRobotsTxt(string t)
		{
			/* robots.txt example file

	            User-agent: *
	            Disallow: /cgi-bin/
	            Disallow: /tmp/
	            Disallow: /private/

	            User-agent : OpenWebSpider
	            Crawl-Delay: 5
	            Disallow: /private/

            */

			/*string singleUserAgentArea = string.Empty;

			var regexExtractUserAgents = new Regex(
				"(?<agents>User-agent[\\s\\t]*:[\\s\\t]*(?<robot>[^\\n\\r]+).*?(?=User-agent[\\s\\t]*:|$))"
				, RegexOptions.IgnoreCase | RegexOptions.Singleline);

			for (int i = 0; i < 2; i++)
			{
				MatchCollection mcUserAgents = regexExtractUserAgents.Matches(t);*/

			/* nel primo ciclo controlla che ci siano i disallow per l'user-agent: "*"
			 * nel secondo ciclo controlla per l'user-agent specificato nel CRAWLER NAME e se c'è sovrascrive gli URL trovati in precedenza
			 */
			/*foreach (Match mMatch in mcUserAgents)
			{
				if (i == 0 &&
					String.Equals(mMatch.Groups["robot"].ToString().Trim(), "*", StringComparison.CurrentCultureIgnoreCase))
				{
					singleUserAgentArea = mMatch.Value;
				}

				if (i == 1 &&
					String.Equals(mMatch.Groups["robot"].ToString().Trim(), OpenWebSpider.NAME,
								  StringComparison.CurrentCultureIgnoreCase))
				{
					singleUserAgentArea = mMatch.Value;
				}
			}
		}*/

			// singleUserAgentArea qui conterrà il testo specifico per uno dei 2 user-agent trovati
			// regex per l'estrazione degli URL dei disallow:
			// disallow\s*:\s*(?<url>[^\n]*)

			/*var regexExtractDisallows = new Regex("disallow\\s*:\\s*(?<url>[^\\n]*)", RegexOptions.IgnoreCase);
			MatchCollection mcDisallows = regexExtractDisallows.Matches(singleUserAgentArea);

			foreach (Match mMatch in mcDisallows)
			{
				string __disallow = mMatch.Groups["url"].ToString().Replace("\r", "").Replace("\n", "").Trim();
				if (__disallow != "")
				{
					robotsTxtDisallows.disallowList.Add(__disallow);
					output.write("   - Disallow [" + __disallow + "] ");
				}
			}

			// estrae il Crawl-Delay
			// Crawl-Delay\s*:\s*(?<crawldelay>[^\D]*)
			var regexExtractCrawlDelay = new Regex("crawl-delay\\s*:\\s*(?<crawldelay>[^\\D]*)", RegexOptions.IgnoreCase);
			MatchCollection mcCrawlDelay = regexExtractCrawlDelay.Matches(singleUserAgentArea);

			foreach (Match mMatch in mcCrawlDelay)
			{
				try
				{
					robotstxtVars.robotstxtCrawlDelay = int.Parse(mMatch.Groups["crawldelay"].ToString());
					output.write("   - Crawl-Delay: " + robotstxtVars.robotstxtCrawlDelay + " seconds ");
				}
				catch
				{
					robotstxtVars.robotstxtCrawlDelay = 0;
				}
			}

			return true;
		}*/
			return false;
		}
	}
}