// ows.cs created with MonoDevelop
// User: shen139 at 13:53 29/06/2008
//

using System;
using System.Threading;
using GlobalVars;
using nsGlobalOutput;

namespace GlobalVars
{
	internal class OpenWebSpider
	{
		public static string NAME = "OpenWebSpider";
		public static string AUTHOR = "Stefano Alimonti (shen139@openwebspider.org)";
		public static string VERSION = "0.1.4";
		public static string ID = "C001";
		public static string USERAGENT;

		// se impostato a true: lo spider verrà terminato in modo pulito
		public static bool stopItGracefully;

		// se impostato a true: lo spider verrà messo in pausa
		public static bool crawlerActPAUSE;
	}
}

namespace OpenWebSpiderCS
{
	public class ows
	{
		private threading threads;

		// init OpenWebSpider
		public ows(int outputType)
		{
			// crea un nuovo oggetto output con destinazione la Console (type == 1) con debugMode attivo
			output.init(outputType, true);

			output.write(OpenWebSpider.NAME + "(v" + OpenWebSpider.VERSION + ") [ http://www.openwebspider.org/ ]");
			output.write("  Copyright (c) 2004-2009 " + OpenWebSpider.AUTHOR + "\n");
			output.write(" + Libraries:");
			output.write("   - Mysql/NET Connector (v5.2.5.0)");
			output.write("   - UltraID3Lib (v0.9.6.6)");
			output.write("   - PDFBox (v0.7.3)\n");
		}

		// legge i parametri passati dalla riga di comando
		public bool readCommandLine(string[] _args)
		{
			// legge gli argomenti dalla linea di comando
			if (args.readArgs(_args) == false)
				return false;


			{
				// controlla che l'URL da indicizzare sia valido
				// se manca l'http iniziale ce lo aggiunge
				if (!args.startURL.StartsWith("http://", StringComparison.CurrentCultureIgnoreCase))
					args.startURL = "http://" + args.startURL;

				var startPage = new page(args.startURL, args.startURL, null);
				if (startPage.isValidPage == false)
				{
					output.write("Invalid URL [" + args.startURL + "]");
					return false;
				}

				// imposta il dominio corrente da indicizzare
				threadsVars.currentDomain = startPage;

				return true;
			}
		}

		// legge il file di configurazione
		public bool readConfFile(string f)
		{
			GlobalVars.readConfFile.parseFile(f);
			if (GlobalVars.readConfFile.isConfFileParsed == false)
			{
				// il file di configurazione non esiste o non è stato letto correttamente
				output.write("Error(1) while reading configuration file.");
				return false;
			}
			return true;
		}

		public bool isMysqlNeeded()
		{
			if (args.stressTest)
				return false;

			return true;
		}

		// connessione ai server mysql
		public bool mysqlConnect()
		{
			output.write("");
			output.write(" + Connecting to MySQL Server [" + GlobalVars.readConfFile.mysqlServerHostList + "]; DB [" +
			             GlobalVars.readConfFile.databaseHostList + "]");

			/* connection string(s) */
			mysqlConn.connHostList = new mysql("Server=" + GlobalVars.readConfFile.mysqlServerHostList + ";" +
			                                   "Database=" + GlobalVars.readConfFile.databaseHostList + ";" +
			                                   "User ID=" + GlobalVars.readConfFile.mysqlUserNameHostList + ";" +
			                                   "Password=" + GlobalVars.readConfFile.mysqlPassWordHostList + ";" +
			                                   "Port=" + GlobalVars.readConfFile.mysqlServerPort1 + ";" +
			                                   "charset=utf8;" +
			                                   "Pooling=false");

			output.write(" + Connecting to MySQL Server [" + GlobalVars.readConfFile.mysqlServerPageList + "]; DB [" +
			             GlobalVars.readConfFile.databasePageList + "]");
			mysqlConn.connPageList = new mysql("Server=" + GlobalVars.readConfFile.mysqlServerPageList + ";" +
			                                   "Database=" + GlobalVars.readConfFile.databasePageList + ";" +
			                                   "User ID=" + GlobalVars.readConfFile.mysqlUserNamePageList + ";" +
			                                   "Password=" + GlobalVars.readConfFile.mysqlPassWordPageList + ";" +
			                                   "Port=" + GlobalVars.readConfFile.mysqlServerPort2 + ";" +
			                                   "charset=utf8;" +
			                                   "Pooling=false");

			if (!mysqlConn.connHostList.isConnected || !mysqlConn.connPageList.isConnected)
			{
				// impossibile connettersi ad uno dei mysql server
				output.write("   - Error(2) while trying to connect to one or more mysql server.");
				return false;
			}
			else
				output.write("   - Connected to both MySQL Servers");

			output.write("");

			return true;
		}

		/* showBanner
		 * mostra un cartello con le impostazioni dello spider all'avvio
		 */

		public void showBanner()
		{
			output.write("");
			output.write(" + Parameters:");
			output.write("   - Starting URL     : " + threadsVars.currentDomain.GenerateURL());
			output.write("   - Single Host Mode : " + (args.singleHostMode ? "On" : "Off"));
			output.write("   - Crawl-Delay      : " + args.crawlDelay + " seconds");
			output.write("");
		}

		// inizia l'indicizzazione
		public bool startCrawling()
		{
			// inizializza le liste degli URL da indicizzare e esterni(cache) + la lista delle relazioni
			urlsLists.init();
			externUrlsLists.init();
			relsList.init();
			imagesLists.init();

			// inizializza i thread
			threads = new threading();

			bool haveToIndexSomething = true;

			// funzioni per l'accesso al DBs
			var __db = new db();

			// attiva l'handler per la pressione di CTRL+C
			output.handleCTRLC();

			while (haveToIndexSomething)
			{
				// svuota le liste
				output.write(" + Clearing structures...");
				urlsLists.clear();
				externUrlsLists.l.Clear();
				relsList.rels.Clear();
				imagesLists.clear();

				output.write("");

				// init mutexes
				threads.initMutexes();

				if (isMysqlNeeded())
				{
					// *******************************************************************
					output.write(" + Checking connection to MySQL servers...");
					if (mysqlConn.connHostList.ping() == false || mysqlConn.connPageList.ping() == false)
					{
						output.write("   - One or both MySQL server disconnected, trying to reconnect!");
						if (mysqlConnect() == false)
						{
							output.write("   - Unable to connect to one or both MySQL server!");
							break;
						}
					}
					// *******************************************************************

					// inserisce/aggiorna nel DB il sito corrente e ritorna l'hostID
					threadsVars.currentDomain._hostID = __db.startIndexThisSite(threadsVars.currentDomain);
				}

				// a questo punto ha aggiunto l'HOST al DB: possiamo uscire
				if (args.add2Hostlist)
				{
					output.write("\n\n");
					output.write(" + Host added to the table of the hosts!");
					output.write("   - Hostname : " + threadsVars.currentDomain._hostname);
					output.write("   - Host ID  : " + threadsVars.currentDomain._hostID);
					output.write("\n\n");
					break;
				}

				if (isMysqlNeeded())
				{
					// carica i limiti dal DB [ hostlist_extras ] se presenti
					limits.loadHostlistExtraLimits(threadsVars.currentDomain._hostID);
				}

				limits.showLimits();

				// setta il tempo esatto di inizio dell'indicizzazione del dominio corrente
				limits.startTime = DateTime.Now.Ticks;

				// azzera le pagine, i bytes indicizzati e gli errori HTTP
				limits.curPages = 0;
				limits.curBytes = 0;
				limits.curErrorCodes = 0;

				// se siamo in stress test non controllare il file robots.txt
				if (args.stressTest == false)
				{
					// recupera e analizza il file: robots.txt
					{
						var prb = new robots();
						var tmpHttpRobotsTxt = new http();
						string tmpsRobotsTxt;
						tmpsRobotsTxt =
							tmpHttpRobotsTxt.getURL(
								"http://" + threadsVars.currentDomain._hostname + ":" + threadsVars.currentDomain._port + "/robots.txt", null,
								false);
						if (tmpHttpRobotsTxt.statusCode == 200)
							prb.parseRobotsTxt(tmpsRobotsTxt);
						else
							output.write("   - robots.txt not found");
					}
				}

				output.write("");

				// aggiunge al primo posto della lista la prima pagina da indicizzare
				urlsLists.addURL(threadsVars.currentDomain);

				// crea i threads che indicizzeranno le pagine
				try
				{
					threads.createThreads();
				}
				catch (Exception e)
				{
					output.write("   - Error while creating threads: " + e.Message);
					threads.killThreads();
					return false;
				}

				try
				{
					bool waitThreads = true;

					while (waitThreads)
					{
						Thread.Sleep(250);

						__db.checkCrawlerAct();

						// se abbiamo premuto CTRL+C : esci dal ciclo di indicizzazione
						if (OpenWebSpider.stopItGracefully)
							waitThreads = false;

						// controlla ogni tot millisecondi se ci sono altre pagine da indicizzare
						// se no: passa al prossimo dominio

						threadsVars.mutexAccessURLList.WaitOne();
						try
						{
							if (urlsLists.getPageByStatus(0) == null && urlsLists.getPageByStatus(2) == null)
								waitThreads = false;
						}
						catch (Exception e)
						{
							output.write("   - Error 1 [Wait Threads]: " + e.Message);
						}
						finally
						{
							threadsVars.mutexAccessURLList.ReleaseMutex();
						}

						// controlla i limiti
						if (limits.checkLimits() == false)
							waitThreads = false;
					}
				}
				catch (Exception e)
				{
					output.write("   - Error 2 [Wait Threads]: " + e.Message);
				}

				// killa i threads
				threads.killThreads();


				// *******************************************************************
				// ri-controlla la connessione al mysql e se down prova a ristabilirla
				if (isMysqlNeeded())
				{
					output.write(" + Checking connection to MySQL servers...");
					if (mysqlConn.connHostList.ping() == false || mysqlConn.connPageList.ping() == false)
					{
						output.write("   - One or both MySQL server disconnected, trying to reconnect!");
						if (mysqlConnect() == false)
						{
							output.write("   - Unable to connect to one or both MySQL server!");
							break;
						}
					}
				}

				// *******************************************************************

				// aggiorna nel DB il sito corrente ("indicizzato"; status = 1)
				__db.stopIndexThisSite(threadsVars.currentDomain);

				// stampa le statistiche di indicizzazione
				printCurDomainStats();

				if (isMysqlNeeded())
				{
					// lascia intatti i duplicati?
					if (!args.keepDup)
					{
						// elimina le pagine con MD5 duplicato lasciando solo quella con id minore
						__db.deleteDuplicatedPages(threadsVars.currentDomain._hostID);
					}

					// riversa gli URL esterni trovati nel DB
					__db.swapExternURLs2DB();

					// salva le relazioni nel DB
					__db.saveRels();

					//salva le immagini
					__db.saveImages();

					// calcolo del rank delle pagine
					new rank(threadsVars.currentDomain._hostID);
				}

				// c'è stata una pressione di CTRL+C : esci
				if (OpenWebSpider.stopItGracefully)
					break;

				// abbiamo indicizzato il primo sito! dobbiamo continuare?
				if (args.singleHostMode)
					haveToIndexSomething = false; // siamo in single host mode; non indicizzare nient'altro
				else
				{
					// recupera il primo host libero dal DB
					page p = __db.getFirstAvailableURL();

					if (p == null)
					{
						// impossibile trovare un URL: esci!!!
						haveToIndexSomething = false;

						output.write(
							" + Nothing to do: no available website to index!!! (Try to add: --add-external to the command line arguments) ");
					}
					else
					{
						// imposta il dominio corrente da indicizzare
						threadsVars.currentDomain = p;
						haveToIndexSomething = true;
					}
				}
			}

			return true;
		}

		// spegne lo spider
		public void shutDown()
		{
			if (isMysqlNeeded())
			{
				// sconnette i mysql server
				mysqlConn.connHostList.disconnect();
				mysqlConn.connPageList.disconnect();
				output.write(" + MySQL Servers Disconnected! ");
			}

			output.write("");

			output.write("Bye bye");

			output.write("");
		}

		public void printCurDomainStats()
		{
			output.write("\n + Stats ");
			output.write("   - Hostname: " + threadsVars.currentDomain._hostname + "  [Port: " + threadsVars.currentDomain._port +
			             "]");
			output.write("   - Pages Indexed: " + limits.curPages + " [" + limits.curBytes/1024 + " Kb] in " +
			             ((DateTime.Now.Ticks - limits.startTime)/10000000) + " seconds");
			output.write("\n");
		}
	}
}