using System;
using System.Security.Cryptography;
using System.Text;

namespace OpenWebSpiderCS
{
	public class md5
	{
		public string compute(string text)
		{
			// calcola l'md5 della pagina (text2Index)
			Byte[] originalBytes;
			Byte[] encodedBytes;
			MD5 md5;

			md5 = new MD5CryptoServiceProvider();
			originalBytes = Encoding.Default.GetBytes(text);
			encodedBytes = md5.ComputeHash(originalBytes);

			var s = new StringBuilder();
			foreach (byte b in encodedBytes)
			{
				s.Append(b.ToString("x2").ToLower());
			}

			return s.ToString();
		}
	}
}