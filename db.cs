// db.cs created with MonoDevelop
// User: shen139 at 15:05 29/06/2008
//

using System;
using System.Text.RegularExpressions;
using System.Threading;
using GlobalVars;
using nsGlobalOutput;

namespace GlobalVars
{
	public class dbLimits
	{
		// lunghezze massime dei campi sul DB
		public static int maxHostnameLength = 100;
		public static int maxPageLength = 255;
		public static int maxTitleLength = 255;
		public static int maxAnchorText = 255;
	}
}

namespace OpenWebSpiderCS
{
	public class db
	{
		public string myMySQLEscapeString(string s)
		{
			return s.Replace("\\", "\\\\").Replace("'", "\\'");
		}

		public page getFirstAvailableURL()
		{
			int hostID;
			string url;

			try
			{
				if (readConfFile.sqlWhereHostlist == "")
					readConfFile.sqlWhereHostlist = "1=1";

				// prende l'ID del primo url libero
				hostID =
					int.Parse(mysqlConn.connHostList.getValueFromTable("hostlist", "id",
					                                                   "status=0 AND ( " + readConfFile.sqlWhereHostlist +
					                                                   " ) ORDER by priority DESC"));
				if (hostID > 0)
				{
					// setta l'url come "in scansionamento"
					mysqlConn.connHostList.executeSQLQuery("UPDATE hostlist SET status = 2, crawler_id = '" +
					                                       myMySQLEscapeString(OpenWebSpider.ID) + "' WHERE id = " + hostID);

					// prende l'url
					url = mysqlConn.connHostList.getValueFromTable("hostlist", "CONCAT('http://', hostname, ':', port)",
					                                               "id = " + hostID);

					output.write("\n\n\n");

					output.write(" + Indexing new domain:  " + url);

					output.write("\n");

					var ret = new page(url, url, null);
					ret._hostID = hostID;

					return ret;
				}
			}
			catch
			{
				return null;
			}

			return null;
		}

		public int GetHostId(page p)
		{
			int hostID;

			try
			{
				// prende l'ID del primo url libero
				hostID =
					int.Parse(mysqlConn.connHostList.getValueFromTable("hostlist", "id",
					                                                   " hostname = '" + p._hostname + "' AND port = " + p._port + " "));
				if (hostID > 0)
					return hostID;
			}
			catch
			{
			}

			return 0;
		}

		/* startIndexThisSite
		 * inserisce il dominio corrente nel DB se non esiste altrimenti aggiorna lo stato(=2)
		 * ritorna l'hostID
		 */

		public int startIndexThisSite(page p)
		{
			// prova a recuperare l'host_id dal DB
			if (p._hostID == 0)
				p._hostID = GetHostId(p);

			// definisce lo stato "Indicizzazione in corso"
			int status = 2;

			// se siamo in sola aggiunta dell'host: mettilo come "Da indicizzare"
			if (args.add2Hostlist)
				status = 0;

			// se l'host non è presente nel DB: inseriscilo
			if (p._hostID == 0)
			{
				mysqlConn.connHostList.executeSQLQuery(
					"INSERT IGNORE INTO hostlist (hostname, port, status, lastvisit, crawler_id) VALUES('" +
					myMySQLEscapeString(p._hostname) + "', " + p._port + ", " + status + ", curdate(), '" +
					myMySQLEscapeString(OpenWebSpider.ID) + "')");
				p._hostID = GetHostId(p);
			}
			else
				mysqlConn.connHostList.executeSQLQuery("UPDATE hostlist SET status = " + status +
				                                       ", lastvisit=curdate(), crawler_id = '" +
				                                       myMySQLEscapeString(OpenWebSpider.ID) + "' WHERE id = " + p._hostID +
				                                       " limit 1");

			// se siamo in sola aggiunta dell'host: non eliminare l'indice
			if (args.add2Hostlist)
				return p._hostID;


			// == eliminazione record dell'indice ==

			// a questo punto hostID è sicuramente settato
			// lo usiamo per eliminare le vecchie pagine indicizzate e le relazioni
			if (p._hostID != 0)
			{
				// elimina le pagine
				mysqlConn.connPageList.executeSQLQuery("DELETE FROM pages where host_id = " + p._hostID);

				// elimina le relazioni
				mysqlConn.connHostList.executeSQLQuery("DELETE FROM rels where host_id = " + p._hostID);

				// se stiamo indicizzando le immagini: le rimuove
				if (args.indexImages)
					mysqlConn.connPageList.executeSQLQuery("DELETE FROM images where src_host_id = " + p._hostID);

				// se stiamo indicizzando gli MP3: rimuovili
				/*if (args.indexMP3)
					mysqlConn.connPageList.executeSQLQuery("DELETE FROM mp3 where host_id = " + p._hostID);*/

				// se stiamo indicizzando i PDF: rimuovili
				if (args.indexPDF)
					mysqlConn.connPageList.executeSQLQuery("DELETE FROM pdf where host_id = " + p._hostID);
			}

			return p._hostID;
		}

		/* stopIndexThisSite
		 * imposta il dominio corrente come: "indicizzato" (status=1)
		 */

		public bool stopIndexThisSite(page p)
		{
			if (p._hostID > 0)
				mysqlConn.connHostList.executeSQLQuery(" UPDATE hostlist SET " +
				                                       "  status = 1 " +
				                                       ", indexed_pages = " + limits.curPages +
				                                       ", time_sec = " + ((DateTime.Now.Ticks - limits.startTime)/10000000) +
				                                       ", bytes_downloaded = " + limits.curBytes +
				                                       ", error_pages = " + limits.curErrorCodes +
				                                       ", crawler_id = '' " +
				                                       " WHERE id = " + p._hostID + " limit 1");
			else
				return false;

			return true;
		}

		public bool indexThisPage(page p, html h)
		{
			// se negli argomenti è specificato di non indicizzare: esci
			if (args.noIndex)
				return false;

			string text2Index = string.Empty;

			try
			{
				// controlla che le regex permettano l'indicizzazione di questa pagina
				// 1. stiamo usano hostlist_extras?
				if (limits.useHostlist_Extras_limits)
				{
					// 2. c'è una regex valida per le inclusioni?
					if (limits.he_regex_include_pages != "")
					{
						var testIncludePageRegex = new Regex(limits.he_regex_include_pages, RegexOptions.IgnoreCase);
						// se la pagina corrente NON VERIFICA l'espressione regolare: non indicizzare
						if (!testIncludePageRegex.IsMatch(p._page))
							return false;
					}

					// 3. c'è una regex valida per le esclusioni?
					if (limits.he_regex_exclude_pages != "")
					{
						var testExcludePageRegex = new Regex(limits.he_regex_exclude_pages, RegexOptions.IgnoreCase);
						// se la pagina corrente VERIFICA l'espressione regolare: non indicizzare
						if (testExcludePageRegex.IsMatch(p._page))
							return false;
					}
				}
			}
			catch (Exception e)
			{
				output.write("\n\n + Error while parsing hostlist_extras regex: " + e.Message + "\n\n");
			}

			if (p.isValidPage && h.HTML != "")
			{
				if (h.contentType.StartsWith("text/html", StringComparison.CurrentCultureIgnoreCase))
				{
					text2Index = h.UnHTML(h.HTML);
				}
				else if (h.contentType.StartsWith("text/", StringComparison.CurrentCultureIgnoreCase))
				{
					// indicizza il testo (es.: *.txt; *.c; *.h)
					text2Index = h.removeUnWantedChars(h.HTML).Trim();
				}
				else
					return false;

				return addContent2Index(p._hostID, p._hostname, p._page, p._title, p._anchorText, p._depthLevel, text2Index, h.HTML);
			}
			return false;
		}

		public bool addContent2Index(int i_host_id, string i_hostname, string i_page, string i_title, string i_anchorText,
		                             uint i_depthLevel, string i_text, string i_cache)
		{
			bool ret = true;
			var MD5 = new md5();

			string sql = "INSERT INTO pages SET " +
						 "  host_id = " + threadsVars.currentDomain._hostID +
			             ", hostname = '" + myMySQLEscapeString(i_hostname) + "'" +
			             ", page='" + myMySQLEscapeString(i_page) + "'" +
			             ", title='" + myMySQLEscapeString(i_title) + "'" +
			             ", anchor_text='" + myMySQLEscapeString(i_anchorText) + "'" +
			             ", date=curdate(),time=curtime()" +
			             ", level=" + i_depthLevel +
			             ",`text`= '" + myMySQLEscapeString(i_text) + "'";


			sql += ",`html_md5`= '" + MD5.compute(i_text) + "' ";


			if (args.cachingMode == 1)
				sql += ",`cache`= '" + myMySQLEscapeString(i_cache) + "'";
			else if (args.cachingMode == 2)
				sql += ",`cache`= COMPRESS('" + myMySQLEscapeString(i_cache) + "')";

			threadsVars.mutexMySQLPageList.WaitOne();
			try
			{
				ret = mysqlConn.connPageList.executeSQLQuery(sql);
			}
			catch (Exception e)
			{
				output.write("SQL Error: " + e.Message + "\n\nSQL: -===[\n" + sql.Substring(0, 1000) + "\n]===-\n\n");
				ret = false;
			}
			finally
			{
				threadsVars.mutexMySQLPageList.ReleaseMutex();
			}
			return ret;
		}

		/* swapExternURLs2DB
		 * riversa gli URL esterni trovati nel DB
		 */

		public bool swapExternURLs2DB()
		{
			output.write(" + Swapping External URLs to DB [ " + externUrlsLists.l.Count + " ]...");

			if (externUrlsLists.isInitialized && externUrlsLists.l.Count > 0)
			{
				foreach (page __p in externUrlsLists.l)
				{
					mysqlConn.connHostList.executeSQLQuery("INSERT IGNORE INTO hostlist (hostname, port, status, lastvisit) " +
					                                       "VALUES('" + __p._hostname + "', " + __p._port + ", 0, null)");
				}
				return true;
			}

			return false;
		}

		/* saveRels
		 * salva le relazioni su DB
		 */

		public bool saveRels()
		{
			output.write(" + Saving rels [ " + relsList.rels.Count + " ]...");

			foreach (relsList.node __nodeInList in relsList.rels)
			{
				if (__nodeInList.__linkedPage._hostID == 0)
					__nodeInList.__linkedPage._hostID = GetHostId(__nodeInList.__linkedPage);

				// aggiunge solo URL di cui esiste un host_id
				// può esistere nel caso non si aggiungano host esterni
				if (__nodeInList.__linkedPage._hostID > 0)
				{
					if (args.relsMode == 1)
					{
						mysqlConn.connHostList.executeSQLQuery(
							"INSERT IGNORE INTO rels (host_id, page, linkedhost_id, linkedpage, textlink) " +
							"VALUES('" + __nodeInList.__page._hostID + "', '/', " + __nodeInList.__linkedPage._hostID + ", '/', '" +
							myMySQLEscapeString(__nodeInList.__linkedPage._anchorText) + "')");
					}
					else if (args.relsMode == 2)
					{
						mysqlConn.connHostList.executeSQLQuery(
							"INSERT IGNORE INTO rels (host_id, page, linkedhost_id, linkedpage, textlink) " +
							"VALUES('" + __nodeInList.__page._hostID + "', '" + myMySQLEscapeString(__nodeInList.__page._page) + "', " +
							__nodeInList.__linkedPage._hostID + ", '" + myMySQLEscapeString(__nodeInList.__linkedPage._page) + "', '" +
							myMySQLEscapeString(__nodeInList.__linkedPage._anchorText) + "')");
					}
				}
			}

			return false;
		}

		/* deleteDuplicatedPages
		 * 
		 * elimina tutte le pagine per l'host_id corrente che hanno un MD5 duplicato lasciando solo la pagina con id minore
		 * 
		 * es.: se indicizza www.example.com/ e www.example.com/index.html -> rimuoverà: www.example.com/index.html 
		 *
		 * 
		 * delete from pages 
		 * where host_id = 2104 and id not in 
		 * (
		 *   select id from view_unique_pages where host_id = 2104
		 * )
		 * 
		 * 
		 * =======================
		 * 
		 * CREATE
		 * VIEW `ows_index`.`view_unique_pages` 
		 * AS
		 * (
		 *		select min(id) as id, host_id
		 *		from pages
		 *		group by html_md5
		 * )
		 * 
		 */

		public void deleteDuplicatedPages(int host_id)
		{
			output.write(" + Deleting duplicated pages...");

			mysqlConn.connPageList.executeSQLQuery(" DELETE FROM pages " +
			                                       " WHERE host_id = " + host_id + " AND id NOT IN " +
			                                       " ( " +
			                                       "   SELECT id FROM view_unique_pages WHERE host_id = " + host_id +
			                                       " )");
			return;
		}

		/* saveImages
		 * riversa le immagini trovati nel DB
		 */

		public bool saveImages()
		{
			if (args.indexImages == false)
				return false;

			output.write(" + Saving images [ " + imagesLists.l.Count + " ]...");

			if (imagesLists.isInitialized && imagesLists.l.Count > 0)
			{
				foreach (imagesLists.imageStruct __is in imagesLists.l)
				{
					// l'host id dell'immagine non è settato?
					if (__is.imagePage._hostID == 0)
					{
						__is.imagePage._hostID = GetHostId(__is.imagePage);

						// se l'host_id è ancora 0 allora dobbiamo inserire l'host in hostlist
						if (__is.imagePage._hostID == 0)
						{
							mysqlConn.connHostList.executeSQLQuery(
								"INSERT IGNORE INTO hostlist (hostname, port, status, lastvisit) VALUES('" +
								myMySQLEscapeString(__is.imagePage._hostname) + "', " + __is.imagePage._port + ", 0, curdate())");
							__is.imagePage._hostID = GetHostId(__is.imagePage);
						}
					}

					// qui l'host_id deve per forza essere impostato! se non lo è c'è stato un errore!
					if (__is.imagePage._hostID > 0)
					{
						mysqlConn.connPageList.executeSQLQuery(
							"INSERT INTO images (src_host_id, src_page, image_host_id, image, alt_text, title_text) " +
							"VALUES(" + __is.srcPage._hostID + ", " +
							"'" + myMySQLEscapeString(__is.srcPage._page) + "', " +
							__is.imagePage._hostID + ", " +
							"'" + myMySQLEscapeString(__is.imagePage._page) + "', " +
							"'" + myMySQLEscapeString(__is.alt_text) + "', " +
							"'" + myMySQLEscapeString(__is.title_text) + "')");
					}
				}
				return true;
			}

			return false;
		}

		/* indexPDF
         * indicizza le informazioni estratte dal file PDF
         */

		public bool indexPDF(page p, int pdfSize, string pdf2text)
		{
			string outStr;
			string sql;
			bool ret;

			outStr = "\n";
			outStr += " + Indexing PDF [ " + p.GenerateURL() + " ]\n";

			output.write(outStr);

			var htmp = new html();

			pdf2text = htmp.removeUnWantedChars(pdf2text);

			addContent2Index(p._hostID, p._hostname, p._page, p._title, p._anchorText, p._depthLevel, pdf2text, pdf2text);

			sql = "INSERT INTO pdf (host_id, filename, pdf_size, pdf_text) " +
			      "VALUES(" + p._hostID + ", " +
			      "'" + myMySQLEscapeString(p._page) + "', " +
			      "'" + pdfSize + "', " +
			      "'" + myMySQLEscapeString(pdf2text) + "') ";


			threadsVars.mutexMySQLPageList.WaitOne();
			try
			{
				ret = mysqlConn.connPageList.executeSQLQuery(sql);
			}
			catch (Exception e)
			{
				output.write("SQL Error: " + e.Message + "\n\nSQL: -===[\n" + sql.Substring(0, 1000) + "\n]===-\n\n");
				ret = false;
			}
			finally
			{
				threadsVars.mutexMySQLPageList.ReleaseMutex();
			}

			return ret;
		}

		/* indexMP3
         * indicizza le informazioni estratte dal file MP3
         */

		public bool indexMP3(page p, mp3 MP3Info)
		{
			string outStr;
			string sql;
			bool ret = false;

			outStr = "\n";
			outStr += " + Indexing MP3 [ " + p.GenerateURL() + " ]\n";
			outStr += "   - Title   : " + MP3Info.mp3Title + "\n";
			outStr += "   - Artist  : " + MP3Info.mp3Artist + "\n";
			outStr += "   - Album   : " + MP3Info.mp3Album + "\n";
			outStr += "   - Genre   : " + MP3Info.mp3Genre + "\n";
			outStr += "   - Duration: " + MP3Info.mp3Length + " seconds\n";
			outStr += "\n";

			output.write(outStr);

			var existSql =
				"SELECT host_id FROM mp3 WHERE host_id = " + threadsVars.currentDomain._hostID + " && " +
				"filename = '" + myMySQLEscapeString(p._page) + "' && " +
				"mp3_size = '" + MP3Info.mp3Size + "' && " +
				"mp3_artist = '" + myMySQLEscapeString(MP3Info.mp3Artist) + "' && " +
				"mp3_title = '" + myMySQLEscapeString(MP3Info.mp3Title) + "' && " +
				"mp3_album = '" + myMySQLEscapeString(MP3Info.mp3Album) + "' && " +
				"mp3_genre = '" + myMySQLEscapeString(MP3Info.mp3Genre) + "' && " +
				"mp3_duration = '" + MP3Info.mp3Length + "' && " +
				"mp3_bitrate = '" + MP3Info.mp3Bitrate + "' && " +
				"mp3_original_url = '" + myMySQLEscapeString(MP3Info.mp3OriginalUri.ToString()) + "'";

			sql =
				"INSERT INTO mp3 (host_id, filename, mp3_size, mp3_artist, mp3_title, mp3_album, mp3_genre, mp3_duration, mp3_bitrate, mp3_original_url,`date`) " +
				"VALUES(" + threadsVars.currentDomain._hostID + ", " +
				"'" + myMySQLEscapeString(p._page) + "', " +
				"'" + MP3Info.mp3Size + "', " +
				"'" + myMySQLEscapeString(MP3Info.mp3Artist) + "', " +
				"'" + myMySQLEscapeString(MP3Info.mp3Title) + "', " +
				"'" + myMySQLEscapeString(MP3Info.mp3Album) + "', " +
				"'" + myMySQLEscapeString(MP3Info.mp3Genre) + "', " +
				MP3Info.mp3Length + "," +
				MP3Info.mp3Bitrate + "," +
				"'" + myMySQLEscapeString(MP3Info.mp3OriginalUri.ToString()) + "', NOW())";


			threadsVars.mutexMySQLPageList.WaitOne();
			try
			{
				// Save the mp3 if it doesn't exist in the database
				bool exists = true;
				using (var result = mysqlConn.connPageList.getSQLResult(existSql))
				{
					if (result == null || !result.Read())
					{
						exists = false;
					}
				}

				if(!exists) {
					ret = mysqlConn.connPageList.executeSQLQuery(sql);
				}
			}
			catch (Exception e)
			{
				output.write("SQL Error: " + e.Message + "\n\nSQL: -===[\n" + sql.Substring(0, 1000) + "\n]===-\n\n");
				ret = false;
			}
			finally
			{
				threadsVars.mutexMySQLPageList.ReleaseMutex();
			}

			return ret;
		}


		/* checkCrawlerAct
         * controlla se c'è da fare qualcosa
         * 
         * GlobalVars.mysqlConn.connHostList non è protetto da mutex perchè non ci sono altri thread che lo usano
         * 
         */

		public void checkCrawlerAct()
		{
			int act = 0;
			try
			{
				act =
					int.Parse(mysqlConn.connHostList.getValueFromTable("crawler_act", "act",
					                                                   " crawler_id = '" + myMySQLEscapeString(OpenWebSpider.ID) + "' "));
			}
			catch
			{
				act = 0;
			}
			finally
			{
				if (act == 1) /* exit */
				{
					// setta il segnale di uscita
					OpenWebSpider.stopItGracefully = true;

					//aspetta un secondo per permettere a altri crawler con lo stesso crawler_id di leggere l'info
					Thread.Sleep(1000);

					//mette uno stato neutro
					mysqlConn.connHostList.executeSQLQuery("UPDATE crawler_act SET act = 0 WHERE crawler_id = '" +
					                                       myMySQLEscapeString(OpenWebSpider.ID) + "' LIMIT 1 ");
				}
				else if (act == 2) /* pause */
				{
					OpenWebSpider.crawlerActPAUSE = true;
				}
				else
				{
					// 0 o riga inesistente
					// togli azioni in sospeso
					OpenWebSpider.crawlerActPAUSE = false;
				}
			}
			return;
		}
	}
}