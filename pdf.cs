using org.pdfbox.pdmodel;
using org.pdfbox.util;

/*
 * TEST PDFs:
        http://lab.openwebspider.org/ext/test_ext.htm
 *
 */

/*
 * Using PDFBox in .NET requires adding references to:
 * 
 *   - PDFBox-0.7.3.dll
 *   - IKVM.GNU.Classpath
 *  and copying IKVM.Runtime.dll to the bin directory.
 */

namespace OpenWebSpiderCS
{
	public class pdf
	{
		public string errorMessage = "";
		public bool isValidPdf;
		public string pdfText = "";

		public pdf(string filename)
		{
			/* PDFBox-0.7.3 */
			PDDocument doc = PDDocument.load(filename);
			var stripper = new PDFTextStripper();
			pdfText = stripper.getText(doc);

			return;
		}
	}
}