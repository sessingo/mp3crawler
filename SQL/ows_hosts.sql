/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`ows_hosts` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `ows_hosts`;

/*Table structure for table `crawler_act` */

DROP TABLE IF EXISTS `crawler_act`;

CREATE TABLE `crawler_act` (
  `crawler_id` varchar(10) NOT NULL,
  `act` int(10) unsigned NOT NULL,
  UNIQUE KEY `NewIndex1` (`crawler_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `hostlist` */

DROP TABLE IF EXISTS `hostlist`;

CREATE TABLE `hostlist` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `hostname` varchar(100) NOT NULL,
  `port` int(6) NOT NULL default '80',
  `status` int(11) NOT NULL default '0',
  `crawler_id` varchar(11) NOT NULL,
  `lastvisit` varchar(10) default NULL,
  `indexed_pages` int(11) unsigned NOT NULL default '0',
  `time_sec` int(11) unsigned NOT NULL default '0',
  `bytes_downloaded` int(11) unsigned NOT NULL default '0',
  `error_pages` int(10) unsigned NOT NULL default '0',
  `priority` int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (`hostname`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `hostlist_extras` */

DROP TABLE IF EXISTS `hostlist_extras`;

CREATE TABLE `hostlist_extras` (
  `host_id` int(10) unsigned NOT NULL,
  `max_pages` int(10) unsigned NOT NULL default '0',
  `max_level` int(10) NOT NULL default '-1',
  `max_seconds` int(10) unsigned NOT NULL default '0',
  `max_bytes` int(10) unsigned NOT NULL default '0',
  `max_HTTP_errors` int(10) unsigned NOT NULL default '0',
  `include_pages_regex` varchar(250) NOT NULL,
  `exclude_pages_regex` varchar(250) NOT NULL,
  PRIMARY KEY  (`host_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*Table structure for table `rels` */

DROP TABLE IF EXISTS `rels`;

CREATE TABLE `rels` (
  `host_id` int(10) unsigned NOT NULL,
  `page` varchar(255) NOT NULL,
  `linkedhost_id` int(10) unsigned NOT NULL,
  `linkedpage` varchar(255) NOT NULL,
  `textlink` varchar(255) character set utf8 default NULL,
  UNIQUE KEY `unique_index` (`host_id`,`page`,`linkedhost_id`,`linkedpage`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
