<html>
<head>
<title>OpenWebSpider - Open Source PHP Search Engine Example</title>
<META NAME="ROBOTS" CONTENT="INDEX, NOFOLLOW">
<style>
	#rnd_container {background: #FFFFFF; margin:1px;}
    
    .rnd_top, .rnd_bottom {display:block; background:#FFFFFF; font-size:1px;}
    .rnd_b1, .rnd_b2, .rnd_b3, .rnd_b4 {display:block; overflow:hidden;}
    .rnd_b1, .rnd_b2, .rnd_b3 {height:1px;}
    .rnd_b2, .rnd_b3, .rnd_b4 {background:#FFFFFF; border-left:1px solid #3300FF; border-right:1px solid #3300FF;}
    .rnd_b1 {margin:0 5px; background:#3300FF;}
    .rnd_b2 {margin:0 3px; border-width:0 2px;}
    .rnd_b3 {margin:0 2px;}
    .rnd_b4 {height:2px; margin:0 1px;}
    
    .rnd_content
	{
		display:block;
		border:0 solid #3300FF;
		border-width:0 1px;
		background:#FFFFFF;
		color:#000000;
	}
</style>
</head>
<body vlink="#0000FF" alink="#0000FF"> 
<?php require("vars.php"); ?> 
<?php require("funct.php"); ?> 
<center> 
  <p> <a href="index.php"><img src="img/ows_logo.png" style="border:0px;" alt="OpenWebSpider LOGO Example"></a> </p> 
  <p>  
  <div id="rnd_container" style="width:600px;"> <b class="rnd_top"><b class="rnd_b1"></b><b class="rnd_b2"></b><b class="rnd_b3"></b><b class="rnd_b4"></b></b> 
    <div class="rnd_content" style="padding:40px;"> 
      <form action="index.php" method="get"> 
        <input maxLength="100" size="60" value="<?php echo htmlspecialchars(stripslashes($_GET["q"])); ?>" name="q"> 
        <input type="submit" value="Search" > 
      </form> 
    </div> 
    <b class="rnd_bottom"><b class="rnd_b4"></b><b class="rnd_b3"></b><b class="rnd_b2"></b><b class="rnd_b1"></b></b> </div> 
  </p> 
</center> 
<?php
	$search_query = stripslashes($_GET["q"]);
	$page = $_GET["p"];
	if( $search_query != "" )
	{
			?> 
<div style="text-align:left;"> 
  <div id="rnd_container" style="90%;"> <b class="rnd_top"><b class="rnd_b1"></b><b class="rnd_b2"></b><b class="rnd_b3"></b><b class="rnd_b4"></b></b> 
    <div class="rnd_content" style="padding:5px; "> 
      <?php
		ows_search($search_query, $page, $server, $user, $pass, $db2, $Logging, $ResultsPerPage);
			?> 
    </div> 
    <b class="rnd_bottom"><b class="rnd_b4"></b><b class="rnd_b3"></b><b class="rnd_b2"></b><b class="rnd_b1"></b></b> </div> 
</div> 
<?php
	}
?> 
  <br>
  <br>
  <hr />
  <center>
	<div style="color:#666666;">
		<a href="crawler_admin.php">Admin Running Crawlers</a> | Powered by <a href="http://www.openwebspider.org/" target="_blank">OpenWebSpider</a>
	</div>
	</center>
</body>
</html>
