<?php

function owssize($owsQuery)
{
	$tmpowsQuery=" " . trim($owsQuery) . " " ;
	if(strstr($tmpowsQuery," \"")==FALSE && strstr($tmpowsQuery," +")==FALSE && strstr($tmpowsQuery," -")==FALSE && strstr($tmpowsQuery,"<")==FALSE && strstr($tmpowsQuery,">")==FALSE && strstr($tmpowsQuery,"* ")==FALSE)
	{
		if(strstr($owsQuery," ")==FALSE)
		{
			$tmpowsQuery="\\\"".trim($owsQuery)."\\\"";
			$owsQuery=$tmpowsQuery;
		}
		else
		{
			$tmpowsQuery="+\\\"".trim($owsQuery);
			$owsQuery=str_replace(" ","\\\" +\\\"",$tmpowsQuery);
			$owsQuery.="\\\"";
		}
	}
	return $owsQuery;
}

function DrawPages($page,$results,$resultsperpage,$query)
{
	$lbound=0;
	$rbound=0;
	
	if($page<0)
		$page=0;
		
	if($page-10<0) $lbound=0; else $lbound=$page-10;
	
	if($page+10>$results/$resultsperpage)
		$rbound=$results/$resultsperpage;
	else
		$rbound=$page+10;
	
	for($i=$lbound;$i<$rbound;$i++)
	{
		if($i!=$page)
			printf("<font face=\"Arial\"><a href=\"index.php?q=%s&p=%d\">%d</a></font>\r\n",urlencode($query),$i,$i+1);
		else
			printf("<font face=\"Arial\"><b>%d </b></font>",$i+1);
	}
}

function microtime_float()
{
	list($usec, $sec) = explode(" ", microtime());
return ((float)$usec + (float)$sec);
}

function ows_search($search,$page,$server,$user,$pass,$database,$log,$ResultsPerPage)
{
	$my_search=mysql_escape_string($search);
	
	$db = mysql_connect($server, $user, $pass);
	if ($db == FALSE)
		die ("Error 1 [mysql_connect()]: Can't connect to mysql server! [Please edit vars.php]");
		
	mysql_select_db($database, $db)
		or die ("Error 2 [mysql_select_db()]");
	
	if($page=="")
		$page=0;
	
	$start = microtime_float();
	
	$query = "select count(*) from pages where match(`text`) against(\"".owssize($my_search)."\") + match(`title`) against(\"".owssize($my_search)."\") + match(`anchor_text`) against(\"".owssize($my_search)."\") + match(`page`) against(\"".owssize($my_search)."\") + match(`hostname`) against(\"".owssize($my_search)."\") ";
	$result = mysql_query($query, $db);
	$row = mysql_fetch_array($result);
	$resultsfound=$row[0];
	
	if( $page>$resultsfound/$ResultsPerPage)
		$page=(int)abs($resultsfound/$ResultsPerPage);
	
	$query = "select hostname,page,`title`,match(`text`) against(\"".owssize($my_search)."\") + match(`title`) against(\"".owssize($my_search)."\") + match(`anchor_text`) against(\"".owssize($my_search)."\") + match(`page`) against(\"".owssize($my_search)."\") + match(`hostname`) against(\"".owssize($my_search)."\") as relevancy, id,rank, LEFT(`text`, 400),level from pages having relevancy > 0 order by relevancy DESC,rank DESC limit ".($page*$ResultsPerPage).",".$ResultsPerPage ;
	$result = mysql_query($query, $db);
	$nRes= mysql_num_rows($result);
	$endtime = abs(number_format(microtime_float()-$start,3));
	// echo $query;
	
	?>
 <div align="center"> 
  <table border="0" width="100%" cellspacing="0" cellpadding="0"> 
     <tr> 
      <td style="border: 0px solid ; font-size: 10px; font-family: Verdana;" width="100%" height="3" bgcolor="#99FFCC"> Query:
         <?php echo htmlspecialchars($search); ?> 
         -
         <?php echo $resultsfound; ?> 
         results found in
         <?php echo $endtime; ?> 
         seconds </td> 
    </tr> 
   </table> 
</div> 
<?php
	
	while ($row = mysql_fetch_array($result))
	{
		$mText=strtolower(htmlspecialchars($row[6]));
		$count=str_word_count($search);
		$words=str_word_count(strtolower($search),1);
		
		for($i=0;$i<$count;$i++)
		{
			$new_text=str_replace($words[$i],"<strong>".$words[$i]."</strong>", $mText);
			$mText=$new_text;
		}
		?> 
<table border="0" width="100%" cellspacing="0" cellpadding="0"> 
  <tr> 
    <td style="border: 0px solid ; font-size: 10px; font-family: Verdana;" width="100%" height="3" bgcolor="#EAFEFF"> <B><a href="http://<?php echo"$row[0]$row[1]" ?>"> 
      <?php echo htmlspecialchars($row[2]); ?> 
      </a></B> <br> 
      <?php echo $mText; ?> 
      ... <br> 
      <font color="#006600">Relevancy:
      <?php echo number_format($row[3],2); ?> 
      - Level: <?php echo"$row[7]" ?> - Rank: <?php echo"$row[5]" ?> <font face="Arial" size="1"><a href="cache.php?page=<?php echo"$row[4]" ?>">Get cached version</a></font></font> 
      </p> 
  </tr> 
</table> 
<hr style="margin: 10px 30px;" size="1" align="center" color="#00ccff" noshade="noshade">
<?php
	}
	
	echo "<p align=\"center\">";
	DrawPages($page,$resultsfound,$ResultsPerPage,htmlspecialchars($search));
	echo "</p>";
	
	$ip=getenv("REMOTE_ADDR");
	
	if($log==1)
	{
		$var=fopen("query.log","a");
		
		if($var!=NULL)
		{
			$tm=strftime ("%A %d %B %Y %H:%M");
			$log= "$tm\t\t$ip\t\t$search\r\n";
			fwrite($var,$log);
			fclose($var);
		}
	}
	else
	if($log=2)
	{
		$query = "INSERT INTO querylist (query,results,date,time,ipaddr) VALUES('$my_search',$resultsfound,curdate(),curtime(),'$ip')";
		$result = mysql_query($query, $db);
	}
	
	mysql_close($db);
}
?> 
