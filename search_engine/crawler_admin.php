<html>
<head>
<title>OpenWebSpider - Crawlers Control Panel</title>
<style>
#rnd_container {background: #FFFFFF; margin:1px; width:600px;}
    
    .rnd_top, .rnd_bottom {display:block; background:#FFFFFF; font-size:1px;}
    .rnd_b1, .rnd_b2, .rnd_b3, .rnd_b4 {display:block; overflow:hidden;}
    .rnd_b1, .rnd_b2, .rnd_b3 {height:1px;}
    .rnd_b2, .rnd_b3, .rnd_b4 {background:#FFFFFF; border-left:1px solid #9999FF; border-right:1px solid #9999FF;}
    .rnd_b1 {margin:0 5px; background:#9999FF;}
    .rnd_b2 {margin:0 3px; border-width:0 2px;}
    .rnd_b3 {margin:0 2px;}
    .rnd_b4 {height:2px; margin:0 1px;}
    
    .rnd_content {
    display:block;
    border:0 solid #9999FF;
    border-width:0 1px;
    padding: 4px;
    background:#FFFFFF;
    color:#000000;
	text-align:left;
}
</style>
</head>
<body vlink="#0000FF" alink="#0000FF"> 
<?php require("vars.php"); ?> 
<?php require("funct.php"); ?> 
<?php
function setCrawlerAct($db_conn, $crawler)
{
	mysql_query("INSERT IGNORE INTO crawler_act (crawler_id, act) VALUES('" . mysql_escape_string( $crawler ) . "', 0); ", $db_conn);
	echo "Crawler: " . $crawler . " :: ";
	if( $_GET["act"] == "play" )
	{
		echo "Playing...";
		mysql_query("UPDATE crawler_act SET act = 0 WHERE crawler_id = '" . mysql_escape_string( $crawler ) . "' LIMIT 1", $db_conn);
	}
	else if( $_GET["act"] == "pause" )
	{
		echo "Pausing...";
		mysql_query("UPDATE crawler_act SET act = 2 WHERE crawler_id = '" . mysql_escape_string( $crawler ) . "' LIMIT 1", $db_conn);
	}
	else if( $_GET["act"] == "kill" )
	{
		echo "Killing...";
		mysql_query("UPDATE crawler_act SET act = 1 WHERE crawler_id = '" . mysql_escape_string( $crawler ) . "' LIMIT 1", $db_conn);
	}
	echo "<br><br>";
}
?>
<center> 
  <p> <a href="index.php"><img src="img/ows_logo.png" style="border:0px;" alt="OpenWebSpider LOGO Example"></a> </p> 
  <p><em style="color:#999999;">Feature available since OpenWebSpider v0.1.4</em></p>
  <p>  
  <div id="rnd_container"> <b class="rnd_top"><b class="rnd_b1"></b><b class="rnd_b2"></b><b class="rnd_b3"></b><b class="rnd_b4"></b></b> 
    <div class="rnd_content"> 
      <h3>Active Crawlers</h3> 
      <?php
		$db = mysql_connect($server, $user, $pass);
		if ($db == FALSE)
			die ("Error 1 [mysql_connect()]: Can't connect to mysql server");
			
		mysql_select_db($db1, $db)
			or die ("Error 2 [mysql_select_db()]");

		
		if( $_GET["c"]!="" && $_GET["act"]!="")
		{
			// selezionato un crawler
			setCrawlerAct( $db, $_GET["c"] );
		}
		
	
		$query = "select DISTINCT crawler_id FROM hostlist WHERE TRIM( crawler_id ) <> '' ";
		$result = mysql_query($query, $db);
		$nres = mysql_num_rows($result);
		
		echo "<ul>";
		if( $nres == 0 )
			echo "No Active Crawlers";
		
		echo "<li><a href='crawler_admin.php'>Show All Crawlers Activity <em>[Refresh]</em></a></li>";
		
		while ($row = mysql_fetch_array($result))
		{
			echo "<li><a href='crawler_admin.php?c=" . urlencode( $row[0] ) . "'>". $row[0] . "</a></li>";
			if( $_GET["act"]!="" )
			{
				setCrawlerAct( $db, $row[0] );
			}

		}
		echo "</ul>";
		mysql_free_result($result);
		
		
		if( $_GET["c"]!="" )
		{
			$status = "";
			
			$result = mysql_query("SELECT act FROM crawler_act WHERE crawler_id = '" . mysql_escape_string( $_GET["c"] ) . "' LIMIT 1", $db);
			$row = mysql_fetch_array($result);
			if( $row )
			{
				if( $row[0] == "2")
					$status = " [ Paused ]";
				else if( $row[0] == "1")
					$status = " [ Killing ]";
			}
				
			echo "<hr><h1>" . htmlentities( $_GET["c"] ) . $status . "</h1>";
		}
		else
			echo "<hr><h1>All Crawlers</h1>";
		
		?>
		<a href="crawler_admin.php?c=<?php echo urlencode( $_GET["c"] ); ?>"><strong>Status</strong></a> &nbsp; &nbsp; &nbsp; | &nbsp; &nbsp; &nbsp; <a href="crawler_admin.php?c=<?php echo urlencode( $_GET["c"] ); ?>&act=play">Play</a> &nbsp; &nbsp; &nbsp; | &nbsp; &nbsp; &nbsp; <a href="crawler_admin.php?c=<?php echo urlencode( $_GET["c"] ); ?>&act=pause">Pause</a> &nbsp; &nbsp; &nbsp; | &nbsp; &nbsp; &nbsp; <a href="crawler_admin.php?c=<?php echo urlencode( $_GET["c"] ); ?>&act=kill">Kill</a>
		<br>
		<br>
		<strong>Currently indexing:</strong>
		<?php
			
	
		if( $_GET["c"]!="" )
			$query = "select hostname FROM hostlist WHERE crawler_id = '" . mysql_escape_string( $_GET["c"] ) . "' ";
		else
			$query = "select hostname FROM hostlist WHERE status = 2 ";
			
		$result = mysql_query($query, $db);
	
		echo "<ul>";
		
		while ($row = mysql_fetch_array($result))
		{
			echo "<li><a href='http://" . urlencode( $row[0] ) . "'> http://". $row[0] . "</a></li>";
		}
		echo "</ul>";
		mysql_free_result($result);
			
	
		mysql_close($db);

	?> 
    </div> 
    <b class="rnd_bottom"><b class="rnd_b4"></b><b class="rnd_b3"></b><b class="rnd_b2"></b><b class="rnd_b1"></b></b> </div> 
  </p> 
</center> 
  <br>
  <br>
  <hr />
  <center>
	<div style="color:#666666;">
		<a href="crawler_admin.php">Admin Running Crawlers</a> | Powered by <a href="http://www.openwebspider.org/" target="_blank">OpenWebSpider</a>
	</div>
	</center>
</body>
</html>
