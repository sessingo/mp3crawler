// threading.cs created with MonoDevelop
// User: shen139 at 16:54�22/06/2008
//

using System;
using System.Data;
using GlobalVars;
using MySql.Data.MySqlClient;
using nsGlobalOutput;
using OpenWebSpiderCS;

/*
    http://dev.mysql.com/doc/refman/5.1/en/connector-net-ref-mysqlclient.html
 */

namespace GlobalVars
{
	internal class mysqlConn
	{
		public static mysql connHostList;
		public static mysql connPageList;
	}
}

namespace OpenWebSpiderCS
{
	public class mysql
	{
		private IDbConnection dbcon;
		private int iMySQLMAXQueryTimeout = 120; /* seconds */
		public bool isConnected;

		/* costruttore della classe mysql: si connette al server */
		/*
         connectionString =
          "Server=localhost;" +
          "Database=DB;" +
          "User ID=root;" +
          "Password=password;" +
          "Pooling=false";
         */

		public mysql(string connectionString)
		{
			try
			{
				dbcon = new MySqlConnection(connectionString);
				dbcon.Open();
			}
			catch (Exception e)
			{
				output.write(e.Message);
				return;
			}

			/* controlla che la connessione sia effettivamente aperta */
			if (dbcon.State == ConnectionState.Open)
				isConnected = true;

			executeSQLQuery("SET NAMES 'UTF8';");
			executeSQLQuery("SET CHARACTER SET UTF8;");
		}

		/* distruttore della classe: sconnette dal server */

		~mysql()
		{
			disconnect();
		}

		public void disconnect()
		{
			if (dbcon != null)
			{
				dbcon.Close();
				dbcon = null;
			}
		}

		/* ping()
		 * controlla che ci sia ancora la connessione al DB
		 */

		public bool ping()
		{
			bool connState = true;

			if (isConnected == false || !(dbcon.State == ConnectionState.Open))
				connState = false;

			// se la connessione "sembra" aperta: prova a fare una query
			if (connState)
			{
				IDbCommand dbcmd = dbcon.CreateCommand();
				dbcmd.CommandText = "SELECT 1+1";

				dbcmd.CommandTimeout = iMySQLMAXQueryTimeout;

				try
				{
					dbcmd.ExecuteNonQuery();
				}
				catch (Exception e)
				{
					connState = false;
					output.write("   - Error: " + e.Message);
				}
				finally
				{
					dbcmd.Dispose();
					dbcmd = null;
				}
			}

			return connState;
		}

		/* Usage:
         *   string test = conn1.getValueFromTable("table", "field", "id = 139"); 
         */

		public string getValueFromTable(string table, string field, string where)
		{
			string strReturn = string.Empty;

			/* se la connessione � chiusa: stringa vuota */
			if (isConnected == false || !(dbcon.State == ConnectionState.Open))
				return strReturn;

			string sql =
				"SELECT CAST( " + field + " as CHAR )" +
				" FROM " + table +
				" WHERE 1=1 AND " + where +
				" LIMIT 1";

			IDbCommand dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = sql;

			dbcmd.CommandTimeout = iMySQLMAXQueryTimeout;

			try
			{
				IDataReader reader = dbcmd.ExecuteReader();
				if (reader.Read())
				{
					strReturn = reader[0].ToString();
				}
				// clean up
				reader.Close();
				reader = null;
			}
			catch (Exception e)
			{
				/* se c'� un errore nell'SQL o la connessione � caduta: stampa a video l'eccezione */
				output.write("Unable to execute SQL Query: " + sql + "\nError [getValueFromTable()]: " + e.Message + "\n\n\n");
			}
			finally
			{
				dbcmd.Dispose();
				dbcmd = null;
			}

			return strReturn;
		}

		public bool executeSQLQuery(string SQL)
		{
			/* se la connessione � chiusa: stringa vuota */
			if (isConnected == false || !(dbcon.State == ConnectionState.Open))
				return false;

			IDbCommand dbcmd = dbcon.CreateCommand();
			dbcmd.CommandType = CommandType.Text;
			dbcmd.CommandText = SQL;

			dbcmd.CommandTimeout = iMySQLMAXQueryTimeout;

			bool ret = true;
			try
			{
				dbcmd.ExecuteNonQuery();
			}
			catch (Exception e)
			{
				ret = false;

				if (mysqlConn.connHostList == null)
					output.write("GlobalVars.mysqlConn.connHostList = null");

				if (mysqlConn.connPageList == null)
					output.write("GlobalVars.mysqlConn.connPageList = null");

				/* se c'� un errore nell'SQL o la connessione � caduta: stampa a video l'eccezione */
				output.write("Unable to execute SQL Query: " + SQL.Substring(0, 1000) +
				             "\nMysql1 Connected: " + mysqlConn.connHostList.isConnected +
				             "\nMysql2 Connected: " + mysqlConn.connPageList.isConnected +
				             "\nError [executeSQLQuery()]: " + e.Message + "\n\n\n");
			}
			finally
			{
				dbcmd.Dispose();
				dbcmd = null;
			}

			return ret;
		}

		public IDataReader getSQLResult(string sql)
		{
			/* se la connessione � chiusa: stringa vuota */
			if (isConnected == false || !(dbcon.State == ConnectionState.Open))
				return null;

			IDbCommand dbcmd = dbcon.CreateCommand();
			dbcmd.CommandText = sql;

			dbcmd.CommandTimeout = iMySQLMAXQueryTimeout;

			IDataReader reader = null;

			try
			{
				reader = dbcmd.ExecuteReader();
			}
			catch (Exception e)
			{
				reader = null;

				/* se c'� un errore nell'SQL o la connessione � caduta: stampa a video l'eccezione */
				output.write("Unable to execute SQL Query: " + sql + "\nError [getSQLResult()]: " + e.Message + "\n\n\n");
			}
			finally
			{
				dbcmd.Dispose();
				dbcmd = null;
			}

			return reader;
		}
	}
}