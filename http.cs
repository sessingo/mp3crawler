// http.cs created with MonoDevelop
// User: shen139 at 11:30 25/06/2008
//

using System;
using System.IO;
using System.Net;
using System.Text;
using GlobalVars;
using nsGlobalOutput;

namespace OpenWebSpiderCS
{
	public class http
	{
		// indica lo lo status code dell'ultima richiesta HTTP
		// il contenuto della pagina scaricata
		public string HTML;
		public string contentType;
		public string errorString;
		public int statusCode;
		/*
		public http()
		{
			statusCode = 0;			
			errorString = String.Empty;
			contentType = String.Empty;			
		}
		*/

		public string binaryStream2MD5File(page p, BinaryReader binStream, string contentType, string contentLength)
		{
			string appPath = AppDomain.CurrentDomain.BaseDirectory;
			var MD5 = new md5();
			string fullPath = appPath + MD5.compute(p._page);

			output.write("\n + Downloading " + p.GenerateURL() + " ... ");
			output.write("   - Content-Type: " + contentType);
			output.write("   - Content-Length: " + contentLength + " bytes \n");

			try
			{
				int BUFFER_SIZE = 4096;
				var buf = new byte[BUFFER_SIZE];
				var stream = new FileStream(fullPath, FileMode.Create);
				int n = binStream.Read(buf, 0, BUFFER_SIZE);
				while (n > 0)
				{
					stream.Write(buf, 0, n);
					n = binStream.Read(buf, 0, BUFFER_SIZE);
				}
				stream.Close();
			}
			catch (Exception e)
			{
				output.write("Error: " + e.Message);
			}
			finally
			{
				binStream.Close();
			}

			return fullPath;
		}

		public bool deleteFile(string filename)
		{
			// delete file
			try
			{
				var f = new FileInfo(filename);
				f.Delete();
			}
			catch (Exception e)
			{
				output.write("Error: " + e.Message);
				return false;
			}
			return true;
		}

		public string getURL(string URL, page p, bool followRedirects)
		{
			errorString = String.Empty;
			contentType = String.Empty;
			statusCode = 0;
			HTML = String.Empty;

			try
			{
				ServicePointManager.DefaultConnectionLimit = 300;
				var request = WebRequest.Create(URL) as HttpWebRequest;

				// imposta l'user-agent
				request.UserAgent = OpenWebSpider.USERAGENT;
				request.AllowAutoRedirect = true;
				request.MaximumAutomaticRedirections = 10;
				// imposta il timeout (default: un minuto 60.000 ms)
				request.Timeout = args.reqTimeout*1000;

				// segue i redirect
				/*if (followRedirects)
				{
					request.AllowAutoRedirect = true;
					request.MaximumAutomaticRedirections = 5;
				}
				else
					request.AllowAutoRedirect = false;*/

				var response = (HttpWebResponse) request.GetResponse();

				if (response.CharacterSet == null)
					return HTML;

				// Support to Encodings
				Encoding responseEncoding;
				responseEncoding = Encoding.UTF8; //default UTF-8
				if (response.CharacterSet.Trim() != "")
					responseEncoding = Encoding.GetEncoding(response.CharacterSet);

				var sr = new StreamReader(response.GetResponseStream(), responseEncoding);
				var binaryStream = new BinaryReader(response.GetResponseStream());

				statusCode = (int) response.StatusCode;

				contentType = response.Headers["Content-Type"];

				// il content-type di questa pagina è testo?
				if (contentType.StartsWith("text", StringComparison.CurrentCultureIgnoreCase))
					HTML = sr.ReadToEnd();
				else if (args.indexMP3 && contentType.ToLower().Contains("audio/mpeg"))
				{
					// se sappiamo chi è il padre e ha un hostID valido allora indicizza l'MP3
					string fullPath = binaryStream2MD5File(p, binaryStream, response.Headers["Content-Type"],
						                                    response.Headers["Content-Length"]);

					var MP3 = new mp3(fullPath);
					deleteFile(fullPath);
					if(MP3.isValidMp3)
					{
						MP3.mp3Size = int.Parse(response.Headers["Content-Length"]);
						MP3.mp3OriginalUri = response.ResponseUri;
						var __db = new db();
						__db.indexMP3(p, MP3);
					}
				}
				else if (args.indexPDF && contentType.ToLower() == "application/pdf")
				{
					// se sappiamo chi è il padre e ha un hostID valido allora indicizza il PDF
					if (p != null)
						if (p.isValidPage && p._hostID > 0)
						{
							string fullPath = binaryStream2MD5File(p, binaryStream, response.Headers["Content-Type"],
							                                       response.Headers["Content-Length"]);

							var PDF = new pdf(fullPath);
							deleteFile(fullPath);

							var __db = new db();
							__db.indexPDF(p, int.Parse(response.Headers["Content-Length"]), PDF.pdfText);
						}
				}
				else
					HTML = string.Empty;

				// forza l'encoding corrente a UTF-8
				Encoding utf8 = Encoding.Unicode;

				byte[] responseEncodingBytes = responseEncoding.GetBytes(HTML);

				byte[] utf8Bytes = Encoding.Convert(responseEncoding,
				                                    utf8,
				                                    responseEncodingBytes);

				HTML = utf8.GetString(utf8Bytes);

				sr.Close();
			}
			catch (WebException e)
			{
				// TODO: in caso di 404 leggere ugualmente lo stream e ritornare l'HTML

				var response = (HttpWebResponse) e.Response;
				if (response != null)
				{
					// in caso di eccezione: prova a recuperare da qui lo status code
					statusCode = (int) response.StatusCode;

					if (response.StatusCode == HttpStatusCode.Unauthorized)
					{
						string challenge = null;
						challenge = response.GetResponseHeader("WWW-Authenticate");
						if (challenge != null)
							errorString = "The following challenge was raised by the server:" + challenge;
					}
					else
						errorString = "The following WebException was raised : " + e.Message;
				}
				else
					errorString = "Response Received from server was null";
			}
			catch (Exception e)
			{
				errorString = "The following Exception was raised :" + e.Message;
			}

			return HTML;
		}

		public string getURL(page p, bool followRedirects)
		{
			return getURL(p.GenerateURL(), p, followRedirects);
		}
	}
}