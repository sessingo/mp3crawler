using System;
using HundredMilesSoftware.UltraID3Lib;
using nsGlobalOutput;

namespace OpenWebSpiderCS
{
	public class mp3
	{
		public string errorMessage = "";
		public bool isValidMp3;
		public string mp3Album = "";
		public string mp3Artist = "";
		public int mp3Bitrate;
		public string mp3Genre = "";
		public int mp3Length;
		public int mp3Size;
		public string mp3Title = "";
		public Uri mp3OriginalUri;

		public mp3(string filename)
		{
			isValidMp3 = false;
			errorMessage = "";

			// Parsing

			var mp3 = new UltraID3();

			try
			{
				mp3.Read(filename);

				mp3Title = mp3.Title;
				mp3Artist = mp3.Artist;
				mp3Album = mp3.Album;
				mp3Genre = mp3.Genre;
				mp3Length = (int) Math.Round(mp3.Duration.TotalSeconds);

				if (!string.IsNullOrEmpty(mp3Title) || !string.IsNullOrEmpty(mp3Artist))
				{
					mp3Bitrate = mp3.GetMPEGTrackInfo().AverageBitRate;
					isValidMp3 = true;
				}
			}
			catch (Exception e)
			{
				errorMessage = e.Message;
				output.write("Error: " + e.Message);
				return;
			}

			return;
		}
	}
}